# Lynx Development Kit


## Prerequisitos

* Python  >= 3.8
* PySide2 >= 5.14 or PyQt5 >= 5.13
* desktop-file-utils, for application.desktop creation: optional

## Environment variables

LDK defaults to using PySide2 to use PyQt5 set this environment variable, this is read before the config file.

```
export LDK_PREFERRED_BINDING=PyQt5
```

## Config file

Setting bindings via config file, system wide is fetched last.

* User file location = /username/.config/ldk.conf
* System wide location = /etc/ldk.conf

Config file contents.

```
[bindings]
LDK_PREFERRED_BINDING = PyQt5
```

## Using from the command line
With the command line utility you can create a self-contained web wrapper's in a question of seconds.

```
ldk-cli --url https://my-web-app-url  --title Mytitle
```

Creating Desktop files in the user directory ( ~/.local/share/applications ).

```
ldk-cli --url https://slack.com --title Slack --cde --desc "Collaboration software for connected teams."
```

More options.

```
ldk-cli --help
```

## Using Python

```
#!/usr/bin/env python
from LDK.Application import LWebApp

url = "https://my-web-app-url"

webapp = LWebApp(title="Mytitle", online=True, web_contents=url)

webapp.run()
```