from LDK.Utils import bindings
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QDockWidget


class WebView(QWebEngineView):

    def __init__(self, parent=None):
        QWebEngineView.__init__(self, parent)

    def set_inspected_view(self, view=None):
        self.page().setInspectedPage(view.page() if view else None)


class InspectorDock(QDockWidget):

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        title = "Inspector"
        self.setWindowTitle(title)
