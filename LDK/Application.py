import sys
import subprocess
from LDK.Utils import Instance, bindings, getScreenGeometry
from LDK import Settings
from LDK.Widgets import LWindow
from LDK.WebEngine import LWebview
from PyQt5.QtCore import Qt, QCoreApplication
from PyQt5.QtWidgets import QApplication


class LWebApp(QApplication):
    #### Imports: from LDK.Application import LWebApp
    def __init__(self, config=Settings.config(), **app_config):
        super(LWebApp, self).__init__(sys.argv)
        self.config = config
        self.setAAttribute(Qt.AA_UseHighDpiPixmaps)
        self.setAAttribute(Qt.AA_EnableHighDpiScaling)
        self.applicationStateChanged.connect(self._applicationStateChanged_cb)

        for key, value in app_config.items():
            if isinstance(value, dict):
                for subkey, subvalue in app_config[key].items():
                    config[key][subkey] = subvalue
            else:
                config[key] = value

        if config["setAAttribute"]:
            for attr in config["setAAttribute"]:
                self.setAAttribute(attr)

        if config["remote-debug"] or "--remote-debug" in sys.argv:
            sys.argv.append("--remote-debugging-port=9000")

        if config["debug"] or "--dev" in sys.argv:
            print("Debugging On")
            if not config["debug"]:
                config["debug"] = True
        else:
            print("Production Mode On, use (--dev) for debugging")

        # Enable/Disable GPU acceleration
        if not config["disableGPU"]:
            detect_nvidia_pci = subprocess.Popen(
                "lspci | grep -i --color 'vga\|3d\|2d'", stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                shell=True
            )
            nvidia_pci = detect_nvidia_pci.communicate()[0].decode("utf-8").lower()

        if config["disableGPU"]:
            self.disable_opengl()
            print("Disabling GPU, Software Rendering explicitly activated")
        else:
            if nvidia_pci:
                # Detect NVIDIA cards
                if "nvidia" in nvidia_pci:
                    print("NVIDIA falling back to Software Rendering")
                    self.disable_opengl()
            else:
                print(f"Virtual Machine")

        if not self.config['webview']['online'] and self.config['webview']['IPC']:
            from PyQt5.QtWebEngineCore import QWebEngineUrlScheme

            QWebEngineUrlScheme.registerScheme(QWebEngineUrlScheme("ipc".encode()))

    def _applicationStateChanged_cb(self, event):
        view = Instance.retrieve("view")
        page = view.page()
        # TODO freeze view when inactive to save ram
        if event == Qt.ApplicationInactive:
            print("inactive")
        elif event == Qt.ApplicationActive:
            print("active")

    def disable_opengl(self):
        self.setAAttribute(Qt.AA_UseSoftwareOpenGL)

    def setAAttribute(self, attr):
        QCoreApplication.setAttribute(attr, True)

    def run(self):
        Instance.record("view", LWebview(self.config))
        win = Instance.auto("win", LWindow(self.config))

        if self.config['window']["transparent"]:
            from LDK.Utils import JavaScript
            JavaScript.css(
                "body, html {background-color:transparent !important;background-image:none !important;}", "LDK"
            )

        if self.config['webview']["addCSS"]:
            from LDK.Utils import JavaScript
            JavaScript.css(self.config['webview']["addCSS"], "user")
            print("Custom CSS detected")

        if self.config['webview']["runJavaScript"]:
            from LDK.Utils import JavaScript
            JavaScript.send(self.config['webview']["runJavaScript"])
            print("Custom JavaScript detected")

        if self.config['window']["fullScreen"]:
            screen = getScreenGeometry()
            win.resize(screen.width(), screen.height())
        else:
            win.resize(win.default_size("width"), win.default_size("height"))

        win.setFocusPolicy(Qt.WheelFocus)
        win.show()
        win.setFocus()
        win.window_original_position = win.frameGeometry()
        self.exec_()
