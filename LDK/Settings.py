from LDK.Utils import bindings
from PyQt5.QtCore import Qt
from PyQt5.QtWebEngineWidgets import QWebEngineSettings


def config():
    return {
        "debug": False,
        "remote-debug": False,
        "setAAttribute": (),
        "disableGPU": False,
        "window": {
            "title": "Lynx Development Kit",
            "icon": None,
            "backgroundImage": None,
            "setFlags": Qt.Window,
            "setAttribute": (),
            "state": None,
            "fullScreen": False,
            "transparent": False,
            "toolbar": None,
            "menus": None,
            "SystemTrayIcon": False,
            "showHelpMenu": False,
        },
        "webview": {
            "webContents": "https://codesardine.github.io/Lynx-Development-Kit",
            "online": False,
            "urlRules": None,
            "cookiesPath": None,
            "userAgent": None,
            "addCSS": None,
            "runJavaScript": None,
            "IPC": True,
            "MediaAudioVideoCapture": False,
            "MediaVideoCapture": False,
            "MediaAudioCapture": False,
            "Geolocation": False,
            "MouseLock": False,
            "DesktopVideoCapture": False,
            "DesktopAudioVideoCapture": False,
            "injectJavaScript": {
                "JavaScript": None,
                "name": "Application Script"
            },
            "webChannel": {
                "active": False,
                "sharedOBJ": None
            },
            "enabledSettings": (
                QWebEngineSettings.JavascriptCanPaste,
                QWebEngineSettings.FullScreenSupportEnabled,
                QWebEngineSettings.AllowWindowActivationFromJavaScript,
                QWebEngineSettings.LocalContentCanAccessRemoteUrls,
                QWebEngineSettings.JavascriptCanAccessClipboard,
                QWebEngineSettings.SpatialNavigationEnabled,
                QWebEngineSettings.TouchIconsEnabled
            ),
            "disabledSettings": (
                QWebEngineSettings.PlaybackRequiresUserGesture
            )
        }
}
